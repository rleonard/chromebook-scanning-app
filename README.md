# Chromebook Scanning App
An application to quickly inventory the serial numbers of Chromebook devices by scanning their barcodes with an Android script.

## Install/Running ##

### Server ###
The server is written with node.js, a JavaScript server environment.

Node.js needs to be installed, along with socket.io and Express, and then `node app.js` can be run to start the server.
The server defaults to port 8900.

### Webpage ###
The webpage for the application can be loaded with `localhost:8900` on the machine running the server.

### App ###
The Android script (chromebooks.php) can be loaded with [PHP for Android](http://www.phpforandroid.net/).

## Usage ##
1. Once the webpage is loaded on the laptop or desktop, the Android script can be started.
2. When the app asks for the server, the QR code presented on the webpage can be scanned to connect to the server.
3. The app will walk through the process of scanning Chromebooks.  Chromebooks can be continually scanned and set aside for data entry.
4. After the Chromebooks are scanned, their cart number/device (sub-cart) number and school serial code can be entered on the webpage.  In the past, we have had success having one person scan, and the other person enter data on the webpage.
5. Once all the devices are scanned, the app can be closed by scanning the "stop code" or by scanning one device twice in a row.
6. Edits saved on the webpage are immediately entered into the CSV defined in `app.js` (`codes.csv` by default).

## Licence ##
Copyright 2013, 2014 Ryan Leonard  
This program is distributed under the terms of the GNU General Public License.  
This file is part of the Chromebook Scanning App.

The Chromebook Scanning App is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

The Chromebook Scanning App is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Chromebook Scanning App.  If not, see <http://www.gnu.org/licenses/>.