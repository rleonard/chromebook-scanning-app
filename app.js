/*
 * app.js
 * The main server code for the Chromebook scanning application.
 * Copyright 2013, 2014 Ryan Leonard
 * This program is distributed under the terms of the GNU General Public License.
 * This file is part of the Chromebook Scanning App.
 *
 * The Chromebook Scanning App is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * The Chromebook Scanning App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with The Chromebook Scanning App.  If not, see <http://www.gnu.org/licenses/>.
 */

var url = require('url');
var fs = require('fs');

var express = require('express');

var app = express();

app.get('/connect', function(req, res){
	res.send(JSON.stringify({
		success: true
	}));
	io.sockets.emit('scannerConnected', {});
});

app.get('/newCode', function(req, res) {
	var code = url.parse(req.url, true).query.code;
	io.sockets.emit('newCode', {'code': code});
	res.send("");
});

app.get('/save', function(req, res) {
	var GET = url.parse(req.url, true).query;
	var code = GET.code;
	var tag = GET.tag;
	var cart = GET.cart;
	var cartpos = GET.cartPos;
	save(code, tag, cart, cartpos);
	res.send("");
});

app.get('/ip', function(req,res) {
	var os=require('os');
	var ifaces=os.networkInterfaces();
	for (var dev in ifaces) {
	  var alias=0;
	  ifaces[dev].forEach(function(details){
	    if (details.family=='IPv4') {
	    	if(dev === "eth0") {
	    		ipAddress = details.address;
	    	}
	      console.log(dev+(alias?':'+alias:''),details.address);
	      ++alias;
	    }
	  });
	}
	res.send(ipAddress);
});

// catch-all
app.use(function(req,res) {
	var filename = url.parse(req.url).pathname;
	if(url.parse(req.url).pathname === "/") {
		var filename = "/index.html";
	}
	fs.exists(__dirname + filename, function(exists) {
		if(!exists) {
			res.writeHead(404);
			return res.end("File not found.");
		}
		fs.readFile(__dirname + filename,
			function(err, data) {
				if(err) {
					res.writeHead(500);
					return res.end("Error loading file.");
				}

				res.writeHead(200);
				res.end(data);
			}
		);
	});
})

var listener = app.listen(8900);

var io = require('socket.io').listen(listener);

io.sockets.on('connection', function(socket) {
	socket.emit('connected', {});
	socket.on('save', function(data) {
		var code = data.code;
		var tag = data.tag;
		var cart = data.cart;
		var cartpos = data.cartPos;
		save(code, tag, cart, cartpos);
	});
});

function save(code, tag, cart, cartpos) {
	fs.appendFile('codes.csv', code + ',' + tag + ',' + cart + ',' + cartpos + '\n');
}