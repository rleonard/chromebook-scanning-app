<?php
	/*
	 * chromebooks.php
	 * The main Android application code for the Chromebook scanning application.
	 * Copyright 2013, 2014 Ryan Leonard
	 * This program is distributed under the terms of the GNU General Public License.
	 * This file is part of the Chromebook Scanning App.
	 *
	 * The Chromebook Scanning App is free software: you can redistribute
	 * it and/or modify it under the terms of the GNU General Public License
	 * as published by the Free Software Foundation, either version 3 of
	 * the License, or (at your option) any later version.
	 * 
	 * The Chromebook Scanning App is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public License
	 * along with The Chromebook Scanning App.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$fields = array(
		"tag" => "School Tag",
		"cart" => "Cart Number",
		"cartpos" => "Cart Position"
	);
	$format = "csv";

	require_once("Android.php");
	$droid = new Android();

	// Check if running standalone or with a server/computer

	$droid->dialogCreateAlert(
		"Chromebook Scanner",
		"Run standalone?"
	);
	$droid->dialogSetPositiveButtonText("Run Standalone");
	$droid->dialogSetNegativeButtonText("Run with Laptop");
	$droid->dialogShow();
	$result = $droid->dialogGetResponse();
	$server = $result['result']->which === "negative";

	if($server) {
		// Fetch server URL (with QR code)
		$serverURL = $droid->scanBarcode();
		$serverURL = $droid->getClipboard();
		$serverURL = $serverURL["result"];
		$droid->dialogCreateSpinnerProgress(
			"Connecting to server...",
			"Please wait while we connect to the server."
		);
		$serverConfirm = json_decode(file_get_contents($serverURL."/connect"), true);
		if(!$serverConfirm["success"]) {
			$droid->vibrate();
			$droid->dialogDismiss();
			$droid->dialogCreateAlert("Error", "Sorry, I was unable to connect to the server.");
			$droid->dialogSetPositiveButtonText("Quit");
			$droid->dialogShow();
			$droid->makeToast("Program exiting after error.");
			$droid->exit();
			exit;
		}
		$droid->dialogCreateAlert(
			"Chromebook Scanner",
			"Do you want to enter data on the phone or on the computer?"
		);
		$droid->dialogSetNegativeButtonText("On Phone");
		$droid->dialogSetPositiveButtonText("On Computer");
		$droid->dialogShow();
		$response = $droid->dialogGetResponse();
		$enterOnLaptop = $response["result"]->which !== "negative";
	}
	if(!$server) {
		if($format === "csv") {
			$response = "";
			foreach ($fields as $field => $value) {
				$response .= $field . ",";
			}
			$response .= "\n";
		}
		elseif($format === "json") {
			$response = array();
			$responseNum = 0;
		}
	}

	$droid->dialogCreateAlert(
		"Chromebook Scanner",
		"Ready to scan Chromebooks.  Scan the same code twice or the QR stop-code to stop scanning."
	);
	$droid->dialogSetNegativeButtonText("Quit");
	$droid->dialogSetPositiveButtonText("Start Scanning");
	$droid->dialogShow();
	$response = $droid->dialogGetResponse();
	if($response["result"]->which === "negative") {
		exit;
	}

	$lastScanned = "";
	$droid->scanBarcode();
	$scan = $droid->getClipboard();
	$scan = $scan["result"];
	while($scan !== $lastScanned) {

		if($server && $enterOnLaptop) {
			file_get_contents($serverURL."/newCode?code=$scan");
		}
		else {
			if($server) {
				$queryString = $serverURL."/save?code=$scan";
			}
			foreach ($fields as $field => $value) {
				$userResponse = $droid->dialogGetInput($value, "");
				if($server) {
					$queryString .= "&{$field}={$userResponse}";
				}
				if($format === "csv") {
					$response .= $userResponse . ",";
				}
				elseif($format === "json") {
					$response[$responseNum][$field] = $userResponse;
				}
			}
			if($server) {
				file_get_contents($queryString);
			}
			elseif($format === "csv") {
				$response .= "\n";
			}
			elseif($format === "json") {
				$responseNum++;
			}
		}

		$lastScanned = $scan;
		$droid->scanBarcode();
		$scan = $droid->getClipboard();
		$scan = $scan["result"];
		if($scan === "stop") {
			break;
		}
	}

	if(!$server) {
		if($format === "csv") {
			$body = $response;
		}
		elseif($format === "json") {
			$body = json_encode($response);
		}
		$droid->sendEmail("ryan@ryanleonard.us", "Chromebook Codes", $body)
	}